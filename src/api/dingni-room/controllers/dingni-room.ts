/**
 *  dingni-room controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::dingni-room.dingni-room');
