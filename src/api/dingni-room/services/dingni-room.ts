/**
 * dingni-room service.
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::dingni-room.dingni-room');
